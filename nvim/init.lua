-- Uncomment the line bellow when not recognize modules
-- package.path = package.path .. "<fulldiretory>/nvim/?.lua"
require("configs.settings")
require("plugins.plugins")
require("configs.mappings")
