vim.cmd([[ packadd packer.nvim ]])

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use {
        'nvim-telescope/telescope.nvim',
        requires = {{'nvim-lua/plenary.nvim'}}
    }
    use 'tpope/vim-fugitive'
    use {
        'kristijanhusak/vim-dadbod-ui',
        requires = {{'tpope/vim-dadbod'}}
    }
    use 'mattn/emmet-vim'
    use 'vim-syntastic/syntastic'
    use 'neoclide/coc.nvim'
    use 'editorconfig/editorconfig-vim' -- Plugin EditorConfig
    -- Plugin para Prettier
    use {
        'prettier/vim-prettier',
        run = 'yarn install --frozen-lockfile --production',
        cmd = { "Prettier", "PrettierAsync" }  -- carrega sob demanda
    }
end)
