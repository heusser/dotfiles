local function map(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

vim.g.mapleader = ' '

-- Move around splits using Ctrl + {h,j,k,l}
map('n', '<C-h>', '<C-w>h')
map('n', '<C-j>', '<C-w>j')
map('n', '<C-k>', '<C-w>k')
map('n', '<C-l>', '<C-w>l')

map('n', '<leader>e', ':Ex<CR>')
map('n', '<leader>w', ':w<CR>')
map('n', '<leader>c', ':close<CR>')

-- Mappings for database
map('n', '<leader>du', ':DBUIToggle<CR>')
map('n', '<leader>df', ':DBUIFindBuffer<CR>')
map('n', '<leader>dr', ':DBUIRenameBuffer<CR>')
map('n', '<leader>dl', ':DBUILastQueryInfo<CR>')
vim.cmd([[ let g:db_ui_save_location = '~/.config/db_ui' ]])

-- Mappings for telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.git_files, {})
vim.keymap.set('n', '<leader>fr', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

vim.cmd([[inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"]])
