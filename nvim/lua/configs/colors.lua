-- Colors
vim.cmd([[highlight Comment ctermfg=8]])
vim.cmd([[highlight String ctermfg=180]])
vim.cmd([[highlight Constant ctermfg=99]])
vim.cmd([[highlight Identifier ctermfg=111]])
